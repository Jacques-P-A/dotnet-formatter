# Formatter demo
## Purpose
The purpose of this repo is to see how we feel about using [dotnet-format](https://github.com/dotnet/format) in conjunction with `git` pre-commit hooks and VS pre-build scripts. At the moment this repo only manages automated formatting, for instance line spacing and character count (this information is all specified in the .editorconfig file). However, this .editorconfig file can also be configured to handle naming conventions, as well as the severity of naming convention errors, which is pertinent for us.   
Please see [Format Your .NET Code with Git Hooks](https://randulakoralage82.medium.com/format-your-net-code-with-git-hooks-a0dc33f68048) - it contains an article I followed to achieve this repo. 

## How to see the formatting in action
1. Clone the repo
2. Make sure you have `dotnet-format` installed, else:
    `dotnet tool install -g dotnet-format`
3. Open the `LinterTest` solution with Visual Studio
4. Add the following pre-build event, via Project > LinterTest Properties > Build Events:
```
if $(ConfigurationName) == Debug node ../Scripts/init.js
```    
5. Make any change to the `LinterTest` project, i.e. Creating a new file, or modifying an existing file
6. Build the `LinterTest` solution
7. Commit the change and notice the file is edited with respect to `LinterTest/.editorconfig` on pre-commit.

## To add an .editorconfig solution-wide
1. In Visual Studio, in the Solution Explorer, right-click the Solution and click Add > New Item
2. Select .editorconfig (.NET) to create the file; it should appear in a directory called Solution Items in the Solution Explorer.

## To format your code during development
- Install the [Format document on Save](https://marketplace.visualstudio.com/items?itemName=mynkow.FormatdocumentonSave) via Extensions > Manage Extensions > ...


## Useful tidbits
### How to make naming convention errors prevent a project's build
1. Right-click the project in the Solution Explorer > Properties > Code Analysis
2. Enable Enforce CodeStyle on build
3. In the solution's .editorconfig, add the following
```
#promote naming convention warning severity to error
dotnet_diagnostic.IDE1006.severity=error
```

### How .editorconfig custom naming rules work
See [Code-style naming rules](https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/naming-rules#naming-rule-properties) for a more detailed explanation of what will follow. In essence, each custom naming rules needs three components:
- the symbol group, or the symbols to which the rule applies to
- the naming style, or the naming specification associated to the rule
- the severity of violating the rule
What follows is an example of defining a rule which applies to private fields (1), another rule which applies to public fields (2), and a rule which applies to both private and public fields (3), all from our `.editorconfig` file.   
I'll suppose that rule 1 is that private fields begin with `p`, while rule 2 stipulates that public fields begin with `P`, and rule 3 can be that both private and public fields end with a `_`. We can also specify that for the rest of the variable name, excepting the `p` or `P`, the rest is in pascale case.
We first define the symbols, or who the rule will apply to:
```
#Note: don't forget to promote the severity of naming convention error.
dotnet_diagnostic.IDE1006.severity=error

# * Symbol Definitions *
#private fields
dotnet_naming_symbols.private_fields.applicable_kinds            = field
dotnet_naming_symbols.private_fields.applicable_accessibilities  = *
dotnet_naming_symbols.private_fields.required_modifiers          = private

#public fields
dotnet_naming_symbols.public_fields.applicable_kinds            = field
dotnet_naming_symbols.public_fields.applicable_accessibilities  = *
dotnet_naming_symbols.public_fields.required_modifiers          = public
```
Next we define the style, or what we expect the symbols to do. Note that styles can be reused, or shared among different rules.
```
# * Style Definitions *
#begins with p
dotnet_naming_style.begins_with_p_style.capitalization = pascal_case
dotnet_naming_style.begins_with_p_style.required_prefix = p

#begins with P
dotnet_naming_style.begins_with_P_style.capitalization = pascal_case
dotnet_naming_style.begins_with_P_style.required_prefix = P

#ends with _
dotnet_naming_style.ends_with___style.required_suffix = _
```
Now we can define the rules themselves
```
# * Rule Definitions *
#Rule 1: private fields should begin with p
dotnet_naming_rule.private_fields_should_begin_with_p.severity = error
dotnet_naming_rule.private_fields_should_begin_with_p.symbols  = private_fields
dotnet_naming_rule.private_fields_should_begin_with_p.style    = begins_with_p_style

#Rule 2: public fields should begin with a P
dotnet_naming_rule.public_fields_should_begin_with_P.severity = error
dotnet_naming_rule.public_fields_should_begin_with_P.symbols  = public_fields
dotnet_naming_rule.public_fields_should_begin_with_P.style    = begins_with_P_style

#Rule 3: private and public fields should end with a _
dotnet_naming_rule.private_fields_end_with__.severity = error
dotnet_naming_rule.private_fields_end_with__.symbols  = private_fields
dotnet_naming_rule.private_fields_end_with__.style    = ends_with___style

dotnet_naming_rule.public_fields_end_with__.severity = error
dotnet_naming_rule.public_fields_end_with__.symbols  = public_fields
dotnet_naming_rule.public_fields_end_with__.style    = ends_with___style
```



## Helpful resources
- [.editorconfig naming rules](https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/naming-rules#naming-rule-properties)
- [.editorconfig code style formatting rules](https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules)
- [Metadata file not found](https://stackoverflow.com/questions/1421862/metadata-file-dll-could-not-be-found)