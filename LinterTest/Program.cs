﻿using System;
using System.Collections.Generic;

namespace LinterTest
{

   class Program
   {
      static void Main(string[] args)
      {
         TodoList list = new TodoList(new List<Todo> { new Todo("Go to school."), new Todo("Git gut."), new Todo("Bring home the bacon.") });

         list.MarkDone(list[1]);
         foreach (var item in list)
         {
            Console.WriteLine($"{item}");
         }
      }
   }
}
