﻿using System;

namespace LinterTest
{
   public class Todo
   {
      private static int sId = 0;
      private int mId;
      public string description { get; }
      private bool mIsDone = false;
      public bool isDone { get { return mIsDone; } }

      public Todo(string description)
      {
         if (description == "") throw new Exception("Description shouldn't be empty");
         this.description = description;
         this.mId = sId;
         sId++;
      }

      public void MarkDone()
      {
         this.mIsDone = true;
      }

      public bool IsEqual(Todo other)
      {
         return other.mId == this.mId;
      }

      public override string ToString()
      {
         return $"{description} - {(isDone ? "Done" : "Not done")}";
      }


   }
}
