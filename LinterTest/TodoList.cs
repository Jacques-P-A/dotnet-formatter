﻿using System.Collections.Generic;

namespace LinterTest
{
   public class TodoList
   {
      private List<Todo> mTodos = new List<Todo>();

      public int Count
      {
         get
         {
            return this.mTodos.Count;
         }
      }

      public Todo this[int index]
      {
         get
         {
            return this.mTodos[index];
         }
      }

      public IEnumerator<Todo> GetEnumerator()
      {
         return mTodos.GetEnumerator();
      }

      public TodoList()
      { }

      public TodoList(List<Todo> todos)
      {
         mTodos = todos;
      }

      public void AddNewTodo(string description)
      {
         this.mTodos.Add(new Todo(description));
      }

      public void AddNewTodo(Todo todo)
      {
         this.mTodos.Add(todo);
      }

      public bool Contains(Todo item)
      {
         return this.mTodos.Contains(item);
      }

      public void MarkDone(Todo todo)
      {
         int index = this.mTodos.IndexOf(todo);
         this.mTodos[index].MarkDone();
      }

      public override string ToString()
      {
         string message = "Todos:\n";
         foreach (var todo in mTodos)
         {
            message += $"\t{todo}\n";
         }
         return message;
      }
   }
}
