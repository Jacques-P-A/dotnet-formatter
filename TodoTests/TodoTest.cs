﻿using System.Collections.Generic;
using LinterTest;
using Xunit;

namespace TodoTests
{
   public class TodoTest
   {
      private TodoList MockData()
      {
         return new TodoList(new List<Todo> {
                new Todo("Take out the trash"),
                new Todo("Feed the children")
            });
      }

      [Fact]
      public void TodoIsAdded()
      {
         TodoList list = new TodoList();
         Assert.Equal(expected: 0, actual: list.Count);
         Todo newTodo = new Todo("Bring mom flowers");
         list.AddNewTodo(newTodo);
         Assert.Equal(expected: 1, actual: list.Count);
         Assert.True(list.Contains(newTodo));
      }


      [Fact]
      public void TodoIsMarkedAsDone()
      {
         Todo newTodo = new Todo("Take out the trash");
         TodoList list = new TodoList(new List<Todo>
            {
               newTodo
            });
         list.MarkDone(newTodo);
         Assert.Equal(expected: 1, actual: list.Count);
         Assert.True(list.Contains(newTodo));
         Assert.True(list[0].isDone);
      }

   }
}
